#!/usr/bin/env python2
# coding: UTF-8

import sys
import math
import subprocess
import tempfile
import os.path
import os
import time
import csv

max_node_count, node_count_growth_rate, max_size, size_growth_rate, inverter_path, report_path = sys.argv[1:]
max_node_count = int(max_node_count)
node_count_growth_rate = float(node_count_growth_rate)
max_size = int(max_size)
size_growth_rate = float(size_growth_rate)

def generate_node_counts():
    node_counts = [1]
    while True:
        node_count = node_counts[-1]
        if node_count == max_node_count:
            break
        node_count = min(int(math.ceil(node_count * node_count_growth_rate)), max_node_count)
        node_counts.append(node_count)
    return node_counts

def generate_sizes():
    sizes = [1]
    while True:
        size = sizes[-1]
        if size == max_size:
            break
        size = min(int(math.ceil(size * size_growth_rate)), max_size)
        sizes.append(size)
    return sizes

cpuinfo_file = open('/proc/cpuinfo')
try:
    processor_count = 0
    for line in iter(cpuinfo_file):
        processor_count += line.startswith('processor')
finally:
    cpuinfo_file.close()

report = []

matrix_path = tempfile.mktemp()
try:
    inv_matrix_path = tempfile.mktemp()
    try:
        print 'measuring performance of ' + repr(inverter_path) + ':'
        for node_count in generate_node_counts():
            print '\tnode_count ' + str(node_count) + '/' + str(max_node_count)
            for size in generate_sizes():
                print '\t\tsize ' + str(size) + '/' + str(max_size)
                code = subprocess.call([
                    os.path.join(os.path.dirname(__file__), 'generate_matrix.py'),
                    str(size), matrix_path,
                ], universal_newlines=True)
                if code != 0:
                    raise RuntimeError(repr('generate_matrix.py') + 'returned code: ' + str(code))

                start = time.time()

                code = subprocess.call([
                    'mpirun',
                    '-np', str(node_count),
                    os.path.abspath(inverter_path),
                    matrix_path, inv_matrix_path,
                ], env=dict(os.environ, **{
                    'OMP_NUM_THREADS': str(int(math.ceil(processor_count / node_count))),
                }), universal_newlines=True)
                if code != 0:
                    raise RuntimeError(repr(inverter_path) + ' returned code: ' + str(code))

                end = time.time()

                report.append((node_count, size, end - start))
    finally:
        if os.path.exists(inv_matrix_path):
            os.remove(inv_matrix_path)
finally:
    if os.path.exists(matrix_path):
        os.remove(matrix_path)

report_file = open(report_path, 'w')
try:
    report_writer = csv.writer(report_file)
    report_writer.writerows(report)
finally:
    report_file.close()


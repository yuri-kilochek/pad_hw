#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <mpi.h>
#ifdef TRACE
    #include <pcontrol.h>

    #define TRACE_ENTRY(ID) \
        MPI_Pcontrol(TRACEEVENT, "entry", ID, 0, "")
    #define TRACE_EXIT(ID) \
        MPI_Pcontrol(TRACEEVENT, "exit", ID, 0, "")
#else
    #define TRACE_ENTRY(ID) \
        ((void)0)
    #define TRACE_EXIT(ID) \
        ((void)0)
#endif

void die(char const* format, ...) {
    va_list args; va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);

    int initialized;
    MPI_Initialized(&initialized);
    if (initialized) { MPI_Finalize(); }

    exit(EXIT_FAILURE);
}

int compute_column_count(int size, int node_count, int node_index) {
    return size / node_count + (node_index < size % node_count);
}

void swap_rows(double *columns, int size, int column_count, int jj_start, int i_a, int i_b) {
    for (int jj = jj_start; jj < column_count; ++jj) {
        double element = columns[jj * size + i_a];
        columns[jj * size + i_a] = columns[jj * size + i_b];
        columns[jj * size + i_b] = element;
    }
}

void forward_sweep(double *column, double* columns, int size, int column_count, int jj_start, int j) {
    #pragma omp parallel for shared(jj_start, column_count, columns, size, column)
    for (int jj = jj_start; jj < column_count; ++jj) {
        double factor = columns[jj * size + j] /= column[j];
        for (int i = j + 1; i < size; ++i) {
            columns[jj * size + i] -= column[i] * factor;
        }
    }
}

int main(int argc, char *argv[]) {
    if (MPI_Init(&argc, &argv) != MPI_SUCCESS) {
        die("failed to initialize MPI");
    }

    double start = MPI_Wtime();

    #ifdef TRACE
        MPI_Pcontrol(TRACEFILES, NULL, "trace", 0);
        MPI_Pcontrol(TRACELEVEL, 1, 1, 1);
        MPI_Pcontrol(TRACENODE, 1000000, 1, 1);
    #endif

    int node_count;
    if (MPI_Comm_size(MPI_COMM_WORLD, &node_count) != MPI_SUCCESS) { 
        die("failed to get node count");
    }

    int node_index;
    if (MPI_Comm_rank(MPI_COMM_WORLD, &node_index) != MPI_SUCCESS) { 
        die("failed to get node index");
    }

    if (argc < 3) { 
        die((node_index == 0) ? "paths to matrix files required" : "");
    }

    FILE *input_file = NULL;
    if (node_index == 0) {
        char const* input_path = argv[1];
        input_file = fopen(input_path, "r");
        if (!input_file) { die("failed to open input matrix file"); }
    }

    int size;
    if (node_index == 0) {
        if (fscanf(input_file, "%d", &size) < 1) {
            die("failed to read input matrix size");
        }
        if (size == 0) { die("matrix must have non-zero size"); }
    }
    if (MPI_Bcast(&size, 1, MPI_INT, 0, MPI_COMM_WORLD) != MPI_SUCCESS) {
        die((node_index == 0) ? "failed to broadcast input matrix size" : "");
    }

    int column_count = compute_column_count(size, node_count, node_index);

    int *column_offsets = NULL;
    int *column_counts = NULL;
    if (node_index == 0) {
        column_offsets = malloc(node_count * sizeof(column_offsets[0]));
        if (!column_offsets) { die("failed to allocate matrix column offsets"); }

        column_counts = malloc(node_count * sizeof(column_counts[0]));
        if (!column_counts) { die("failed to allocate matrix column counts"); }

        column_offsets[0] = 0;
        column_counts[0] = column_count;
        for (int node_index = 1; node_index < node_count; ++node_index) {
            column_offsets[node_index] = column_offsets[node_index - 1] + column_counts[node_index - 1];
            column_counts[node_index] = compute_column_count(size, node_count, node_index);
        }
    }

    double *columns = malloc(column_count * size * sizeof(columns[0]));
    if (!columns) { die("failed to allocate input matrix columns"); }

    double *buffer = malloc(size * sizeof(buffer[0]));
    if (!buffer) { die("failed to allocate buffer"); }

    TRACE_ENTRY(0);
    for (int i = 0; i < size; ++i) {
        if (node_index == 0) {
            for (int j = 0; j < size; ++j) {
                int node_index = j % node_count;
                int jj = j / node_count;
                double *element = (node_index == 0)
                    ? &columns[jj * size + i]
                    : &buffer[column_offsets[node_index] + jj];
                if (fscanf(input_file, "%lf", element) < 1) {
                    die("failed to read input matrix element at (%d, %d)", i, j);
                }
            }
        }
        if (MPI_Scatterv(buffer, column_counts, column_offsets, MPI_DOUBLE, (node_index == 0) ? MPI_IN_PLACE : buffer, column_count, MPI_DOUBLE, 0, MPI_COMM_WORLD) != MPI_SUCCESS) {
            die((node_index == 0) ? "failed to scatter input matrix row #%d" : "", i);
        }
        if (node_index != 0) {
            for (int jj = 0; jj < column_count; ++jj) {
                columns[jj * size + i] = buffer[jj];
            }
        }
    }
    TRACE_EXIT(0);

    if (node_index == 0) {
        fclose(input_file);
    }

    double *inv_columns = malloc(column_count * size * sizeof(inv_columns[0]));
    if (!inv_columns) { die("failed to allocate inverse output matrix columns"); }

    #pragma omp parallel for shared(column_count, node_index, node_count, size, inv_columns)
    for (int jj = 0; jj < column_count; ++jj) {
        int j = node_index + jj * node_count;
        for (int i = 0; i < size; ++i) {
            inv_columns[jj * size + i] = (i == j);
        }
    }

    for (int j = 0; j < size; ++j) {
        TRACE_ENTRY(1);
        int jj = j / node_count;

        int pivot_i;
        if (node_index == j % node_count) {
            pivot_i = j;
            double pivot_abs_element = fabs(columns[jj * size + pivot_i]);
            for (int i = pivot_i + 1; i < size; ++i) {
                double abs_element = fabs(columns[jj * size + i]);
                if (abs_element > pivot_abs_element) {
                    pivot_i = i;
                    pivot_abs_element = abs_element;
                }
            }
            if (pivot_abs_element == 0) { die("input matrix is singualar"); }
        }
        if (MPI_Bcast(&pivot_i, 1, MPI_INT, j % node_count, MPI_COMM_WORLD) != MPI_SUCCESS) {
            die((node_index == 0) ? "failed to broadcast pivot index in column #%d" : "", j);
        }

        int j_r = node_index + jj * node_count;

        swap_rows(columns, size, column_count, jj + (j_r < j), pivot_i, j);
        swap_rows(inv_columns, size, column_count, 0, pivot_i, j);

        double *column = (node_index == j % node_count)
            ? columns + jj * size
            : buffer;
        if (MPI_Bcast(column + j, size - j, MPI_DOUBLE, j % node_count, MPI_COMM_WORLD) != MPI_SUCCESS) {
            die((node_index == 0) ? "failed to initiate broadcast of column #%d on forward sweep" : "", j);
        }

        forward_sweep(column, columns, size, column_count, jj + (j_r <= j), j);
        forward_sweep(column, inv_columns, size, column_count, 0, j);
        TRACE_EXIT(1);
    }

    for (int j = size - 1; j > 0; --j) {
        TRACE_ENTRY(2);
        int jj = j / node_count;

        double *column = (node_index == j % node_count)
            ? columns + jj * size
            : buffer;
        if (MPI_Bcast(column, j, MPI_DOUBLE, j % node_count, MPI_COMM_WORLD) != MPI_SUCCESS) {
            die((node_index == 0) ? "failed to initiate broadcast of column #%d on reverse sweep" : "", j);
        }

        #pragma omp parallel for shared(column_count, inv_columns, size, column)
        for (int jj = 0; jj < column_count; ++jj) {
            double factor = inv_columns[jj * size + j];
            for (int i = 0; i < j; ++i) {
                inv_columns[jj * size + i] -= column[i] * factor;
            }
        }
        TRACE_EXIT(2);
    }

    FILE *output_file = NULL;
    if (node_index == 0) {
        char const* output_path = argv[2];
        output_file = fopen(output_path, "w");
        if (!output_file) { die("failed to open inverse output matrix file"); }
    }

    if (node_index == 0) {
        if (fprintf(output_file, "%d\n", size) < 0) {
            die("failed to write inverse output matrix size");
        }
    }

    TRACE_ENTRY(3);
    for (int i = 0; i < size; ++i) {
        if (node_index != 0) {
            for (int jj = 0; jj < column_count; ++jj) {
                buffer[jj] = inv_columns[jj * size + i];
            }
        }
        if (MPI_Gatherv((node_index == 0) ? MPI_IN_PLACE : buffer, column_count, MPI_DOUBLE, buffer, column_counts, column_offsets, MPI_DOUBLE, 0, MPI_COMM_WORLD) != MPI_SUCCESS) {
            die((node_index == 0) ? "failed to rather inverse output matrix row #%d" : "", i);
        }
        if (node_index == 0) {
            for (int j = 0; j < size; ++j) {
                int node_index = j % node_count;
                int jj = j / node_count;
                double element = (node_index == 0)
                    ? inv_columns[jj * size + i]
                    : buffer[column_offsets[node_index] + jj];
                if (fprintf(output_file, "%.*lg%c", 12, element, " \n"[j == size - 1]) < 0) {
                    die("failed to write inverse output matrix element at (%d, %d)", i, j);
                }
            }
        }
    }
    TRACE_ENTRY(3);

    if (node_index == 0) {
        fclose(output_file);
    }

    free(inv_columns);

    free(buffer);

    free(columns);

    if (node_index == 0) {
        free(column_counts);
        free(column_offsets);
    }

    double stop = MPI_Wtime();

    MPI_Finalize();

    if (node_index == 0) {
        printf("ELAPSED_TIME %lf\n", stop - start);
    }
	
    return EXIT_SUCCESS;
}

#!/usr/bin/env python2
# coding: UTF-8

import sys
import random

n, path = sys.argv[1:]
n = int(n)

file = open(path, 'w')
try:
    file.write(str(n) + '\n')
    for i in range(n):
        for j in range(n):
            e = random.uniform(-2, +2)
            file.write(str(e) + ' \n'[j == n - 1])
finally:
    file.close()


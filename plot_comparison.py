#!/usr/bin/env python3
# coding: UTF-8

import argparse
from pathlib import Path
import csv
import collections
import numpy as np
import matplotlib
matplotlib.use('AGG')
import matplotlib.pyplot as plt

argparser = argparse.ArgumentParser()
argparser.add_argument('reports', type=Path, nargs='+')
argparser.add_argument('image', type=Path)
args = argparser.parse_args()

def read_curves(path):
    curves = collections.defaultdict(list)
    with path.open('r', newline='') as file:
        for row in csv.reader(file):
            if len(row) == 3:
                tag, *values = row
            else:
                tag, values = None, row
            values = tuple(map(float, values))
            curves[tag].append(values)
    return {t: np.asarray(vs) for t, vs in curves.items()}

plt.figure(figsize=(20, 15))
curve_handles = []
for report_path in args.reports:
    for label, curve in read_curves(report_path).items():
        if label:
            label = str(report_path.with_suffix('').with_suffix('')) + '/' + label
        else:
            label = str(report_path.with_suffix('').with_suffix(''))
        curve_handle, = plt.plot(*curve.T, label=label)
        curve_handles.append(curve_handle)
plt.legend(handles=curve_handles)
plt.savefig(str(args.image), bbox_inches='tight')

#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <float.h>
#include <math.h>

void die(char const* format, ...) {
    va_list args; va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);
    exit(EXIT_FAILURE);
}

int main(int argc, char const* argv[]) {
    struct timeval start;
    if (gettimeofday(&start, NULL)) {
        die("failed to get start time");
    }

    if (argc < 3) { die("paths to matrix files required"); }

    char const* input_path = argv[1];
    FILE* input_file = fopen(input_path, "r");
    if (!input_file) { die("failed to open input matrix file"); }

    size_t size;
    if (fscanf(input_file, "%zu", &size) < 1) {
        die("failed to read input matrix size");
    }
    if (size == 0) { die("matrix must have non-zero size"); }

    double** bimatrix = malloc(size * sizeof(bimatrix[0]));
    if (!bimatrix) { die("failed to allocate bimatrix row list"); }
    for (size_t i = 0; i < size; ++i) {
        bimatrix[i] = malloc(2 * size * sizeof(bimatrix[i][0]));
        if (!bimatrix[i]) { die("failed to allocate bimatrix row #%zu", i); }
        for (size_t j = 0; j < size; ++j) {
            if (fscanf(input_file, "%lf", &bimatrix[i][j]) < 1) {
                die("failed to read input matrix element at (%zu, %zu)", i, j);
            }
        }
        for (size_t j = 0; j < size; ++j) {
            bimatrix[i][size + j] = (i == j);
        }
    }
    fclose(input_file);

    for (size_t k = 0; k < size; ++k) {
        size_t max_i = k;
        for (size_t i = k + 1; i < size; ++i) {
            if (fabs(bimatrix[i][k]) > fabs(bimatrix[max_i][k])) {
                max_i = i;
            }
        }
        double* tmp = bimatrix[k];
        bimatrix[k] = bimatrix[max_i];
        bimatrix[max_i] = tmp;
        if (bimatrix[k][k] == 0) { die("input matrix is singular"); }

        for (size_t i = k + 1; i < size; ++i) {
            double factor = bimatrix[i][k] / bimatrix[k][k];
            for (size_t j = k; j < 2 * size; ++j) {
                bimatrix[i][j] -= bimatrix[k][j] * factor;
            }
        }
    }
    for (size_t k = size - 1; k < size; --k) {
        for (size_t i = 0; i < k; ++i) {
            double factor = bimatrix[i][k] / bimatrix[k][k];
            for (size_t j = size; j < 2 * size; ++j) {
                bimatrix[i][j] -= bimatrix[k][j] * factor;
            }
        }
        for (size_t j = size; j < 2 * size; ++j) {
            bimatrix[k][j] /= bimatrix[k][k];
        }
    }

    char const* output_path = argv[2];
    FILE* output_file = fopen(output_path, "w");
    if (!output_file) { die("failed to open output inverse matrix file"); }

    if (fprintf(output_file, "%zu\n", size) < 0) {
        die("failed to write output inverse matrix size");
    }
    for (size_t i = 0; i < size; ++i) {
        for (size_t j = 0; j < size; ++j) {
            if (fprintf(output_file, "%.*lg%c", DECIMAL_DIG, bimatrix[i][size + j], " \n"[j == size - 1]) < 0) {
                die("failed to write output inverse matrix element at (%zu, %zu)", i, j);
            }
        }
        free(bimatrix[i]);
    }
    free(bimatrix);
    fclose(output_file);

    struct timeval end;
    if (gettimeofday(&end, NULL)) {
        die("failed to get end time");
    }

    double elapsed = (end.tv_sec - start.tv_sec) +
		     (end.tv_usec - start.tv_usec) * 1e-9;
    printf("ELAPSED_TIME %lf\n", elapsed);
	
    return EXIT_SUCCESS;
}

#!/usr/bin/env python2
# coding: UTF-8

import sys
import math
import subprocess
import tempfile
import os.path
import os
import time
import csv

max_size, size_growth_rate, inverter_path, report_path = sys.argv[1:]
max_size = int(max_size)
size_growth_rate = float(size_growth_rate)

def generate_sizes():
    sizes = [1]
    while True:
        size = sizes[-1]
        if size == max_size:
            break
        size = min(int(math.ceil(size * size_growth_rate)), max_size)
        sizes.append(size)
    return sizes

report = []

matrix_path = tempfile.mktemp()
try:
    inv_matrix_path = tempfile.mktemp()
    try:
        print 'measuring performance of ' + repr(inverter_path) + ':'
        for size in generate_sizes():
            print '\tsize ' + str(size) + '/' + str(max_size)
            code = subprocess.call([
                os.path.join(os.path.dirname(__file__), 'generate_matrix.py'),
                str(size), matrix_path,
            ], universal_newlines=True)
            if code != 0:
                raise RuntimeError(repr('generate_matrix.py') + 'returned code: ' + str(code))

            start = time.time()

            code = subprocess.call([
                os.path.abspath(inverter_path),
                matrix_path, inv_matrix_path,
            ], universal_newlines=True)
            if code != 0:
                raise RuntimeError(repr(inverter_path) + ' returned code: ' + str(code))

            end = time.time()

            report.append((size, end - start))
    finally:
        if os.path.exists(inv_matrix_path):
            os.remove(inv_matrix_path)
finally:
    if os.path.exists(matrix_path):
        os.remove(matrix_path)

report_file = open(report_path, 'w')
try:
    report_writer = csv.writer(report_file)
    report_writer.writerows(report)
finally:
    report_file.close()


#FLAGS=-std=c99 -pedantic -Wall -Wextra -g -lm
FLAGS=-std=c99 -pedantic -Wall -Wextra -DNDEBUG -O3 -lm

#MEASURE_PARAMS=3000 1.4
#MEASURE_MPI_PARAMS=4 1.3 ${MEASURE_PARAMS}

#BMSTU_HOST=195.19.33.110
#BMSTU_PORT=2022
#BMSTU_USER=stud002

gauss_jordan: gauss_jordan.c
	cc ${FLAGS} $< -o $@

gauss_jordan_mpi: gauss_jordan_mpi.c
	mpicc ${FLAGS} $< -o $@

gauss_jordan_mpi_trace: gauss_jordan_mpi.c
	mpicc-TV -DTRACE ${FLAGS} $< -o $@

gauss_jordan_mpi_omp: gauss_jordan_mpi_omp.c
	mpicc -fopenmp ${FLAGS} $< -o $@

.PRECIOUS: input_matrix-%.txt
input_matrix-%.txt: generate_matrix.py
	./$< $* $@

trace: trace.job gauss_jordan_mpi_trace input_matrix-1000.txt
	llsubmit $<
	while ! grep -Po '^ELAPSED_TIME .*$$' trace.stdout; do sleep 1; done
	rm -f trace.stdout

trace.ttf: trace
	cp trace trace2
	tracesort trace
	mv trace2 trace

perf-%.job: perf.job.template
	sed s/SIZE/$*/g $< > $@

.PRECIOUS: perf-%.txt
perf-%.txt: perf-%.job gauss_jordan input_matrix-%.txt
	llsubmit $<
	while ! grep -Po '^ELAPSED_TIME .*$$' perf-$*.stdout | grep -Po '\d+(\.\d+)?' > $@; do sleep 1; done
	rm -f perf-$*.stdout

perf_mpi-%.job: perf_mpi.job.template
	sed s/SIZE/$*/g $< > $@

.PRECIOUS: perf_mpi-%.txt
perf_mpi-%.txt: perf_mpi-%.job gauss_jordan_mpi input_matrix-%.txt
	llsubmit $<
	while ! grep -Po '^ELAPSED_TIME .*$$' perf_mpi-$*.stdout | grep -Po '\d+(\.\d+)?' > $@; do sleep 1; done
	rm -f perf_mpi-$*.stdout


perf_mpi_omp-%.job: perf_mpi_omp.job.template
	sed s/SIZE/$*/g $< > $@

.PRECIOUS: perf_mpi_omp-%.txt
perf_mpi_omp-%.txt: perf_mpi_omp-%.job gauss_jordan_mpi_omp input_matrix-%.txt
	llsubmit $<
	while ! grep -Po '^ELAPSED_TIME .*$$' perf_mpi_omp-$*.stdout | grep -Po '\d+(\.\d+)?' > $@; do sleep 1; done
	rm -f perf_mpi_omp-$*.stdout

PERF_SIZES=1 5 10 20 50 100 200 400 700 1000 1300 1600 2000 2500 3000 3600 4300 5000

perf.txt: $(patsubst %,perf-%.txt,${PERF_SIZES})
	rm -f $@
	for f in $^; do echo "$$(grep -Po '\d+' <<< "$$f") $$(cat $$f)" >> $@; done

perf_mpi.txt: $(patsubst %,perf_mpi-%.txt,${PERF_SIZES})
	rm -f $@
	for f in $^; do echo "$$(grep -Po '\d+' <<< "$$f") $$(cat $$f)" >> $@; done

perf_mpi_omp.txt: $(patsubst %,perf_mpi_omp-%.txt,${PERF_SIZES})
	rm -f $@
	for f in $^; do echo "$$(grep -Po '\d+' <<< "$$f") $$(cat $$f)" >> $@; done

perfs: perf.txt perf_mpi.txt perf_mpi_omp.txt

#gauss_jordan.performance.csv: gauss_jordan measure_performance.py generate_matrix.py Makefile
#	./measure_performance.py ${MEASURE_PARAMS} $< $@
#
#gauss_jordan_mpi.performance.csv: gauss_jordan_mpi measure_performance_mpi.py generate_matrix.py Makefile
#	./measure_performance_mpi.py ${MEASURE_MPI_PARAMS} $< $@
#
#gauss_jordan_mpi_omp.performance.csv: gauss_jordan_mpi_omp measure_performance_mpi.py generate_matrix.py Makefile
#	./measure_performance_mpi.py ${MEASURE_MPI_PARAMS} $< $@
#
#performance_comparison.png: plot_comparison.py gauss_jordan.performance.csv gauss_jordan_mpi.performance.csv gauss_jordan_mpi_omp.performance.csv
#	./$^ $@
#
#bmstu_common: measure_performance.py measure_performance_mpi.py generate_matrix.py Makefile
#	for f in $^; do \
#		sshpass -f BMSTU_PASS scp -P ${BMSTU_PORT} $${f} ${BMSTU_USER}@${BMSTU_HOST}:$${f}; \
#	done
#
#bmstu_gauss_jordan.performance.csv: gauss_jordan.c bmstu_common
#	sshpass -f BMSTU_PASS scp -P ${BMSTU_PORT} $< ${BMSTU_USER}@${BMSTU_HOST}:$<
#	sshpass -f BMSTU_PASS ssh -p ${BMSTU_PORT} ${BMSTU_USER}@${BMSTU_HOST} make gauss_jordan.performance.csv
#	sshpass -f BMSTU_PASS scp -P ${BMSTU_PORT} ${BMSTU_USER}@${BMSTU_HOST}:gauss_jordan.performance.csv $@
#
#bmstu_gauss_jordan_mpi.performance.csv: gauss_jordan_mpi.c bmstu_common
#	sshpass -f BMSTU_PASS scp -P ${BMSTU_PORT} $< ${BMSTU_USER}@${BMSTU_HOST}:$<
#	sshpass -f BMSTU_PASS ssh -p ${BMSTU_PORT} ${BMSTU_USER}@${BMSTU_HOST} make gauss_jordan_mpi.performance.csv
#	sshpass -f BMSTU_PASS scp -P ${BMSTU_PORT} ${BMSTU_USER}@${BMSTU_HOST}:gauss_jordan_mpi.performance.csv $@
#
#bmstu_gauss_jordan_mpi_omp.performance.csv: gauss_jordan_mpi_omp.c bmstu_common
#	sshpass -f BMSTU_PASS scp -P ${BMSTU_PORT} $< ${BMSTU_USER}@${BMSTU_HOST}:$<
#	sshpass -f BMSTU_PASS ssh -p ${BMSTU_PORT} ${BMSTU_USER}@${BMSTU_HOST} make gauss_jordan_mpi_omp.performance.csv
#	sshpass -f BMSTU_PASS scp -P ${BMSTU_PORT} ${BMSTU_USER}@${BMSTU_HOST}:gauss_jordan_mpi_omp.performance.csv $@
#
#bmstu_performance_comparison.png: plot_comparison.py bmstu_gauss_jordan.performance.csv bmstu_gauss_jordan_mpi.performance.csv bmstu_gauss_jordan_mpi_omp.performance.csv
#	./$^ $@

clean:
	rm -f gauss_jordan
	rm -f gauss_jordan_mpi
	rm -f gauss_jordan_mpi_omp
	rm -f *.performance.csv
	rm -f performance_comparison.png
	rm -f bmstu_performance_comparison.png


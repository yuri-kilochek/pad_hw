#!/usr/bin/env python
# coding: UTF-8

import sys
import argparse
import numpy as np

argparser = argparse.ArgumentParser()
argparser.add_argument('matrix', type=argparse.FileType('r'))
argparser.add_argument('inverse_matrix', type=argparse.FileType('r'))
args = argparser.parse_args()

def read_matrix(stream):
    size, *elements = stream.read().split()
    size = int(size)
    elements = list(map(float, elements))
    assert len(elements) == size * size, "not enough elements"
    matrix = np.array(elements).reshape((size, size))
    return matrix

matrix = read_matrix(args.matrix)
inverse_matrix = read_matrix(args.inverse_matrix)

def compute_error(matrix, inverse_matrix):
    size = matrix.shape[0]
    return np.sqrt(np.sum((matrix @ inverse_matrix) ** 2)) / (size * size)

sys.exit(int(compute_error(matrix, inverse_matrix) >
             2 * compute_error(matrix, np.linalg.inv(matrix))))
